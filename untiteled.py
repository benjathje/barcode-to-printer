import os
import pdfkit
from base64 import b64encode
from reportlab.lib import units
from reportlab.graphics import renderPM
from reportlab.graphics.barcode import createBarcodeDrawing
from reportlab.graphics.shapes import Drawing

from django.template.loader import render_to_string
from weasyprint import HTML
from weasyprint import default_url_fetcher

def my_fetcher(url):
    if url.startswith('barcode:'):
        barcode = createBarcodeDrawing('Code128', value = url[8:], barWidth = 0.05 * units.inch, fontSize = 20, humanReadable = True)
        drawing_width = 250
        barcode_scale = drawing_width / barcode.width
        drawing_height = barcode.height * barcode_scale
        drawing = Drawing(drawing_width, drawing_height)
        drawing.scale(barcode_scale, barcode_scale)
        drawing.add(barcode, name='barcode')

        return dict(string=drawing.asString('png'),
                    mime_type='image/png')

    else:
        return weasyprint.default_url_fetcher(url)

'''def get_barcode(value, width, barWidth = 0.05 * units.inch, fontSize = 30, humanReadable = True):

    barcode = createBarcodeDrawing('Code128', value = value, barWidth = barWidth, fontSize = fontSize, humanReadable = humanReadable)

    drawing_width = width
    barcode_scale = drawing_width / barcode.width
    drawing_height = barcode.height * barcode_scale

    drawing = Drawing(drawing_width, drawing_height)
    drawing.scale(barcode_scale, barcode_scale)
    drawing.add(barcode, name='barcode')

    return drawing


def get_image():

    barcode = get_barcode(value = 'Pruebosa', width = 600)
    data = b64encode(renderPM.drawToString(barcode, fmt = 'PNG'))
    #print ('<img src="data:image/png;base64,%s">' % data.decode('ascii'))

    return ('<img src="data:image/png;base64,%s">' % data.decode('ascii'))

print (get_image())
'''

htmlstring = render_to_string("Pito.html", {'code': 1234}, request)
html = HTML(string=htmlstring, url_fetcher=my_fetcher,base_url=request.build_absolute_uri())
main_doc = html.render()
pdf = main_doc.write_pdf()


#pdfkit.from_url('http://172.16.0.164:8000/','Print.pdf')

pdfkit.from_file('Pito.html', 'Print.pdf')

#os.system("lpr Print.pdf") #Solo activar con impresora para probar (Impreme a impresora predeterminada)
